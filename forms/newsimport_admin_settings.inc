<?php

/**
 * @file
 * Contains the HFC Newsroom Import module admin form.
 */

/**
 * Administration form.
 */
function newsimport_admin_settings($form, &$form_state) {

  $form['newsimport_endpoints'] = array(
    '#type' => 'fieldset',
    '#title' => t('Migration Endpoints'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['newsimport_endpoints']['newsimport_user_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('User endpoint'),
    '#default_value' => variable_get('newsimport_user_endpoint', NULL),
    '#description' => t('Enter the endpoint for the user list'),
    '#required' => TRUE,
  );
  $form['newsimport_endpoints']['newsimport_file_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('File endpoint'),
    '#default_value' => variable_get('newsimport_file_endpoint', NULL),
    '#description' => t('Enter the endpoint for the file list'),
    '#required' => TRUE,
  );
  $form['newsimport_endpoints']['newsimport_node_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Node endpoint'),
    '#default_value' => variable_get('newsimport_node_endpoint', NULL),
    '#description' => t('Enter the consumer secret. (See news administrator for details.)'),
    '#required' => TRUE,
  );

  $form['newsimport_filesources'] = array(
    '#type' => 'fieldset',
    '#title' => t('Migration File Sources'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['newsimport_filesources']['newsimport_file_source'] = array(
    '#type' => 'textfield',
    '#title' => t('Source Directory'),
    '#default_value' => variable_get('newsimport_file_source', NULL),
    '#description' => t('Enter the full filesystem path to the source files.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
