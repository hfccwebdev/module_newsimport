<?php

/**
 * Map Calendar and Newsfeeds to new site.
 */
function newsimport_mapping_form($form, &$form_state) {

  $calendars = hfccbane_get_calendars();
  $newsfeeds = hfccbane_get_newsfeeds();
  $feeds = [];

  foreach ($calendars as $key => $item) {
    if ($item['active']) {
      $feeds[$key] = 'Calendar: ' . $item['title'];
    }
  }

  foreach ($newsfeeds as $key => $item) {
    if ($item['active']) {
      $feeds[$key] = 'Newsfeed: ' . $item['title'];
    }
  }

  $mappings = db_query("SELECT feed_id, status, tid FROM {newsimport_map_feed}")->fetchAllAssoc("feed_id");

  $terms = db_query("SELECT tid, name FROM {taxonomy_term_data} WHERE vid = 1")->fetchAllAssoc("tid");
  $terms_opts = [];
  foreach ($terms as $tid => $term) {
    $terms_opts[$tid] = $term->name;
  }

  foreach ($feeds as $key => $value) {
    $form[$key] = [
      '#type' => 'fieldset',
      '#title' => t("@value (@key)", ['@value' => $value, '@key' => $key]),
      '#tree' => TRUE,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form[$key]['status'] = [
      '#type' => 'select',
      '#title' => t('Status'),
      '#options' => [0 => 'ignore', 1 => 'private', 2 => 'public'],
      '#default_value' => isset($mappings[$key]->status) ? $mappings[$key]->status : 2,
      '#required' => TRUE,
    ];
    $form[$key]['tid'] = array(
      '#type' => 'select',
      '#title' => t('Tag'),
      '#options' => ['' => '- None -'] + $terms_opts,
      '#default_value' => isset($mappings[$key]->tid) ? $mappings[$key]->tid : NULL,
      '#required' => FALSE,
    );
  }

  $form['submit'] = ['#type' => 'submit', '#value' => t('Save')];

  return $form;
}

/**
 * Submit handler for form.
 */
function newsimport_mapping_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  foreach ($values as $key => $value) {
    if (isset($value['status'])) {
        db_merge('newsimport_map_feed')
          ->key(['feed_id' => $key])
          ->fields(['status' => $value['status'], 'tid' => !empty($value['tid']) ? $value['tid'] : NULL])
          ->execute();
    }
  }
}
