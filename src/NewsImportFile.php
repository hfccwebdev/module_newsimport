<?php

/**
 * The HFC Newsroom file import service.
 */
class NewsImportFile extends NewsImport {

  /**
   * Import files.
   */
  public static function mapUpdate() {

    global $user;
    if ($user->uid == 0) {
      die ("Cannot run as user 0! Use drush --user=NNN" . PHP_EOL);
    }

    $mappings = db_query("SELECT sourceid, destid FROM {newsimport_map_file}")->fetchAllAssoc("destid");
    $files = db_query("SELECT fid, uid, filename, uri FROM {file_managed}")->fetchAllAssoc("uri");

    $source_dir = variable_get('newsimport_file_source', NULL);
    if (empty($source_dir)) {
      watchdog('newsimport', 'Source file directory not set!', WATCHDOG_ERROR);
      return;
    }
    if (substr($source_dir, -1) != '/') {
      $source_dir .= '/';
    }

    $sources = self::fetch('file');

    foreach ($sources as $source) {
      if (empty($mappings[$source['fid']])) {

        $target = preg_replace('|^public://|', 'private://newsroom/', $source['uri']);

        if (!file_exists($target)) {
          $source_file = preg_replace('|^public://|', $source_dir, $source['uri']);
          if (!file_exists($source_file)) {
            $message = 'Source file @source not found!';
            $values = ['@source' => $source_file];
            watchdog('newsimport', $message, $values, WATCHDOG_WARNING);
          }
          else {
            $destination = dirname($target);
            $uid = isset($source['uid']) ? NewsImport::getDest($source['uid'], 'user') : 0;
            echo "{$source['uri']} => $destination";
            // build $destination to include path, but not filename.
            if ($file = self::linkDestination($source_file, [], $destination, $uid)) {
            db_insert("newsimport_map_file")
              ->fields([
                'sourceid' => $source['fid'],
                'destid' => $file->fid,
                'last_imported' => REQUEST_TIME,
              ])
              ->execute();
              echo " FID: {$file->fid}" . PHP_EOL;
            }
            else {
              echo " *** FAIL ***" . PHP_EOL;
            }
          }
        }
      }
    }
  }

  /**
   * Hard link the file instead of copying.
   *
   * There is no option to replace files with this method. Instead, fail if the target exists.
   *
   * @see filefield_sources_save_file()
   * @see file_save_upload()
   */
  private static function linkDestination($filepath, $validators = [], $destination = FALSE, $uid = 0) {

    // Begin building file object.
    $file = new stdClass();
    $file->uid      = $uid;
    $file->status   = 1;
    $file->filename = trim(basename($filepath), '.');
    $file->uri      = $filepath;
    $file->filemime = file_get_mimetype($file->filename);
    $file->filesize = filesize($filepath);

    $extensions = '';
    if (isset($validators['file_validate_extensions'])) {
      if (isset($validators['file_validate_extensions'][0])) {
        // Build the list of non-munged extensions if the caller provided them.
        $extensions = $validators['file_validate_extensions'][0];
      }
      else {
        // If 'file_validate_extensions' is set and the list is empty then the
        // caller wants to allow any extension. In this case we have to remove the
        // validator or else it will reject all extensions.
        unset($validators['file_validate_extensions']);
      }
    }
    else {
      // No validator was provided, so add one using the default list.
      // Build a default non-munged safe list for file_munge_filename().
      $extensions = 'jpg jpeg gif png txt doc docx xls xlsx pdf ppt pptx pps odt ods odp md';
      $validators['file_validate_extensions'] = array();
      $validators['file_validate_extensions'][0] = $extensions;
    }

    if (!empty($extensions)) {
      // Munge the filename to protect against possible malicious extension hiding
      // within an unknown file type (ie: filename.html.foo).
      $file->filename = file_munge_filename($file->filename, $extensions);
    }

    // Rename potentially executable files, to help prevent exploits (i.e. will
    // rename filename.php.foo and filename.php to filename.php.foo.txt and
    // filename.php.txt, respectively). Don't rename if 'allow_insecure_uploads'
    // evaluates to TRUE.
    if (!variable_get('allow_insecure_uploads', 0) && preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
      $file->filemime = 'text/plain';
      $file->uri .= '.txt';
      $file->filename .= '.txt';
      // The .txt extension may not be in the allowed list of extensions. We have
      // to add it here or else the file upload will fail.
      if (!empty($extensions)) {
        $validators['file_validate_extensions'][0] .= ' txt';
        drupal_set_message(t('For security reasons, your upload has been renamed to %filename.', array('%filename' => $file->filename)));
      }
    }

    // If the destination is not provided, use the temporary directory.
    if (empty($destination)) {
      $destination = 'temporary://';
    }

    // Assert that the destination contains a valid stream.
    $destination_scheme = file_uri_scheme($destination);
    if (!$destination_scheme || !file_stream_wrapper_valid_scheme($destination_scheme)) {
      drupal_set_message(t('The file could not be uploaded, because the destination %destination is invalid.', array('%destination' => $destination)), 'error');
      return FALSE;
    }

    // A URI may already have a trailing slash or look like "public://".
    if (substr($destination, -1) != '/') {
      $destination .= '/';
    }

    // Ensure the destination is writable.
    file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

    $file->destination = file_destination($destination . $file->filename, FILE_EXISTS_ERROR);
    // If file_destination() returns FALSE then $replace == FILE_EXISTS_ERROR and
    // there's an existing file so we need to bail.
    if ($file->destination === FALSE) {
      drupal_set_message(t('The file %source could not be uploaded because a file by that name already exists in the destination %directory.', array('%source' => $file->filename, '%directory' => $destination)), 'error');
      return FALSE;
    }

    // Add in our check of the the file name length.
    $validators['file_validate_name_length'] = array();

    // Call the validation functions specified by this function's caller.
    $errors = file_validate($file, $validators);

    // Check for errors.
    if (!empty($errors)) {
      $message = t('The specified file %name could not be uploaded.', array('%name' => $file->filename));
      if (count($errors) > 1) {
        $message .= theme('item_list', array('items' => $errors));
      }
      else {
        $message .= ' ' . array_pop($errors);
      }
      drupal_set_message($message, 'error');
      return FALSE;
    }

    // link requires the actual path to the file, not a uri.
    $file->uri = $file->destination;
    $destpath = drupal_realpath($file->uri);

    if (!@link($filepath, $destpath)) {
      drupal_set_message(t('File upload error. Could not move uploaded file.'), 'error');
      watchdog('file', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $file->filename, '%destination' => $file->uri));
      return FALSE;
    }

    // Set the permissions on the new file.
    // drupal_chmod($file->uri);

    // If we made it this far it's safe to record this file in the database.
    return file_save($file);
  }


}
