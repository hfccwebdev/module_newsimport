<?php

/**
 * The HFC Newsroom node import service.
 */
class NewsImportNode extends NewsImport {

  /**
   * Import files.
   */
  public static function mapUpdate() {

    global $user;
    if ($user->uid == 0) {
      die ("Cannot run as user 0! Use drush --user=NNN" . PHP_EOL);
    }

    $mappings = db_query("SELECT sourceid, destid, last_imported, hash FROM {newsimport_map_node}")->fetchAllAssoc("sourceid");
    $sources = self::fetch('node');
    echo "Found " . count($sources) . " items to process." . PHP_EOL;

    foreach ($sources as $source) {

      $nid = $source['nid'];
      $mapping = !empty($mappings[$nid]) ? $mappings[$nid] : [];
      $hash = md5(drupal_json_encode($source));
      if (empty($mapping) || $mapping->hash !== $hash) {

        $source['flags'] = self::checkFlags($source);

        if ($source['flags']['status'] == '0') {
          // Ignore this item.
          continue;
        }

        if (empty($mapping)) {
          $node = self::createNode();
          $mode = 'create';
        }
        else {
          $node = node_load($mapping->destid);
          $mode = 'update';
        }

        $privacy = ['1' => 'private', '2' => 'public'];
        echo PHP_EOL . t("@s @n @t (@y - @x - !g) @c", [
          '@s' => $source['created'],
          '@n' => $source['nid'],
          '@t' => trim($source['title']),
          '@y' => $source['type'],
          '@x' => $privacy[$source['flags']['status']],
          '!g' => json_encode($source['flags']['tags']),
          '@c' => $mode,
        ]) . PHP_EOL;

        self::mapFields($source, $node);

        $node->revision = 1;
        $node->log = $node->is_new ? t("Created by newsimport tool.") : t("Updated by newsimport tool.");

        node_save($node);
        if (!empty($node->nid)) {
          db_merge('newsimport_map_node')
            ->key(['sourceid' => $source['nid']])
            ->fields(['destid' => $node->nid, 'last_imported' => time(), 'hash' => $hash])
            ->execute();
        }
      }
    }
  }

  /**
   * Map source fields to destination node.
   */
  private static function mapFields($source, $node) {
    $uid = isset($source['uid']) ? NewsImport::getDest($source['uid'], 'user') : 0;
    $node->title = trim($source['title']);
    $node->uid = $uid;
    $node->revision_uid = $uid;
    $node->name = self::getUsername($uid);
    $node->created = $source['created'];
    $node->promote = !(empty($source['promote'])) ? $source['promote'] : 0;

    if (!empty($source['academic_term'])) {
      $node->field_news_academic_term[LANGUAGE_NONE][0]['value'] = $source['academic_term'];
    }

    if (!empty($source['alternative_link'])) {
      $node->field_news_alt_link[LANGUAGE_NONE][0]['url'] = $source['alternative_link'];
    }

    if (!empty($source['body'])) {
      $node->body[LANGUAGE_NONE][0] = $source['body'];
    }
    elseif (!empty($source['full_description'])) {
      $node->body[LANGUAGE_NONE][0] = $source['full_description'];
    }

    if (!empty($source['cut_line'])) {
      $node->field_news_cut_line[LANGUAGE_NONE][0] = $source['cut_line'];
    }
    elseif (!empty($source['short_description'])) {
      $node->field_news_cut_line[LANGUAGE_NONE][0] = $source['short_description'];
    }

    if (!empty($source['event_date'])) {
      $node->field_news_event_date[LANGUAGE_NONE][0] = $source['event_date'];
    }

    if (!empty($source['legacy_id'])) {
      $node->path['alias'] = 'news/items/' . $source['legacy_id'];
    }

    if (!empty($source['link'])) {
      $node->field_news_link[LANGUAGE_NONE][0]['value'] = $source['link'];
    }

    if (!empty($source['location'])) {
      $node->field_news_event_location[LANGUAGE_NONE][0] = $source['location'];
    }

    if (!empty($source['photo_entity'])) {
      $node->field_news_photo[LANGUAGE_NONE][0] = self::buildPhotoEntity($source['photo_entity']);
    }

    if (!empty($source['release_date'])) {
      $node->field_news_release_date[LANGUAGE_NONE][0] = $source['release_date'];
    }

    if (!empty($source['retire_date'])) {
      $node->field_news_retire_date[LANGUAGE_NONE][0] = $source['retire_date'];
    }

    // Set to private if status flag == 1.
    $node->private = $source['flags']['status'] == 1 ? 1 : 0;

    if (!empty($source['flags']['tags'])) {
      foreach ($source['flags']['tags'] as $tag) {
        $node->field_news_tags[LANGUAGE_NONE][] = ['tid' => $tag];
      }
    }
  }

  /**
   * Map audience targets.
   *
   * Returns an array of item status and tags.
   *
   * Status values:
   *   - 0 = ignore
   *   - 1 = private
   *   - 2 = portal
   */
  private static function checkFlags($source) {
    $mappings = &drupal_static(__FUNCTION__);
    if (!isset($mappings)) {
      $mappings = db_query("SELECT feed_id, status, tid FROM {newsimport_map_feed}")->fetchAllAssoc("feed_id");
    }

    $flags = ['status' => 0, 'tags' => []];

    if ($source['type'] == 'news') {
      $feeds = $source['newsfeeds'];
    }
    elseif ($source['type'] == 'event') {
      $feeds = $source['calendars'];
    }

    if (!empty($feeds)) {
      foreach ($feeds as $feed) {
        $id = $feed['value'];
        if ($mappings[$id]->status > $flags['status']) {
          $flags['status'] = $mappings[$id]->status;
        }
        if (!empty($mappings[$id]->tid) && !in_array($mappings[$id]->tid, $flags['tags'])) {
          $flags['tags'][] = $mappings[$id]->tid;
        }
      }
    }
    return $flags;
  }

  /**
   * Create a new empty node.
   */
  private static function createNode() {
    return entity_create('node', [
      'type' => 'news',
      'changed' => REQUEST_TIME,
      'revision' => 1,
      'status' => 1,
      'promote' => 0,
      'comment' => 0,
      'language' => LANGUAGE_NONE,
    ]);
  }

  /**
   * Build the photo entity to return.
   */
  private static function buildPhotoEntity($source) {
    $fid = isset($source['fid']) ? NewsImport::getDest($source['fid'], 'file') : 0;
    if ($fid) {
      if ($file = file_load($fid)) {
        if (!empty($source['alt'])) {
          $file->alt = $source['alt'];
        }
        if (!empty($source['title'])) {
          $file->title = $source['title'];
        }
        return (array) $file;
      }
      else {
        echo "Cannot load destination file $fid" . PHP_EOL;
      }
    }
    else {
      echo "Cannot load matching file for source fid " . $source['fid'] . PHP_EOL;
    }
  }

  /**
   * Look up usernames.
   */
  private static function getUsername($uid) {
    $users = &drupal_static(__FUNCTION__);
    if (empty($users[$uid])) {
      $user = user_load($uid);
      $users[$uid] = $user->name;
    }
    return $users[$uid];
  }
}
