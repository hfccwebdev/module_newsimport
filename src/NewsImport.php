<?php

/**
 * The HFC Newsroom import utility service.
 */
class NewsImport {

  /**
   * Get Destination id.
   */
  public static function getDest($sourceid, $map) {
    if (in_array($map, ['user', 'file', 'node'])) {
      if ($result = db_query("SELECT destid FROM {newsimport_map_${map}} WHERE sourceid = :sourceid", [':sourceid' => $sourceid])->fetchCol()) {
        return reset($result);
      }
    }
    else {
      drupal_set_message(t('Unknown map type %map', ['%map' => $map]), 'error');
    }
  }

  /**
   * Fetch User Names and IDs.
   */
  protected static function fetch($endpoint) {
    if (in_array($endpoint, ['user', 'file', 'node'])) {
      if ($endpoint = variable_get("newsimport_${endpoint}_endpoint", NULL)) {
        return hfccbane_http_request(["resource" => $endpoint]);
      }
      else {
        drupal_set_message(t('Please set news import endpoints.'), 'error');
      }
    }
    else {
      drupal_set_message(t('Unknown endpoint type %endpoint', ['%endpoint' => $endpoint]), 'error');
    }
  }
}
