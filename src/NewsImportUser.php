<?php

/**
 * The HFC Newsroom user import service.
 */
class NewsImportUser extends NewsImport {

  /**
   * Update user mappings from legacy site.
   *
   * Map all unknown users to uid 1.
   */
  public static function mapUpdate() {

    $mappings = db_query("SELECT sourceid, destid FROM {newsimport_map_user}")->fetchAllAssoc("sourceid");
    $users = db_query("SELECT uid, name FROM {users}")->fetchAllAssoc("name");

    $sources = self::fetch('user');

    foreach ($sources as $source) {

      // Map the source user name to destination id.
      if (!empty($users[$source['name']])) {
        $destid = $users[$source['name']]->uid;
      }
      else {
        watchdog('newsimport', 'Cannot match user %name', ['%name' => $source['name']]);
        $destid = 1;
      }

      if (!empty($mappings[$source['uid']])) {
        if ($destid !== $mappings[$source['uid']]->destid) {
          db_update("newsimport_map_user")
            ->fields([
              'destid' => $destid,
              'last_imported' => REQUEST_TIME,
            ])
            ->condition('sourceid', $source['uid'])
            ->execute();
        }
      }
      else {
        db_insert("newsimport_map_user")
          ->fields([
            'sourceid' => $source['uid'],
            'destid' => $destid,
            'last_imported' => REQUEST_TIME,
          ])
          ->execute();
      }
    }
  }
}
